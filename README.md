Builder
https://www.vogella.com/tutorials/DesignPatternBuilder/article.html

Adapter
https://www.vogella.com/tutorials/DesignPatternAdapter/article.html

Composite
https://dzone.com/articles/composite-design-pattern-java-0

Decorator pattern
https://javapapers.com/design-patterns/decorator-pattern/

Chain of responsability
https://www.tutorialspoint.com/design_pattern/chain_of_responsibility_pattern.htm

Observer Pattern
https://www.journaldev.com/1739/observer-design-pattern-in-java