package com.cegeka.designpatterns.structural.adapter;

public class GermanPlug implements GermanPlugConnector {
	
	public GermanPlug() {
		System.out.print("German Plug + ");
	}
	
	@Override
	public void giveElectricity() {
	}
}
