package com.cegeka.designpatterns.structural.adapter;


/*
 * 



The adapter pattern describes how to convert an object into another object which a clients expects. 
This pattern mainly adapts one object to another one. 
Adapters allow objects to work together that couldn�t otherwise because of incompatible interfaces.
Adapter allows to reuse existing coding without changing it, as the adapter ensures the conversion between the different interfaces.
In comparison to a decorator pattern, 
the adapter pattern only converts objects, while the decorator pattern adds new functionality to an existing object. 
Therefore, the decorator does not change the existing interface.

 */
public class Main {

	public static void main(String[] args) {
		//german plug
		GermanPlugConnector plugConnector = new GermanPlug();	
		
		//introduce german plug in the adaptor
		UKPlugConnector ukAdapter = new GermanToUKPlugConnectorAdapter(plugConnector);
		
		//uk socket 
		UKElectricalSocket electricalSocket = new UKElectricalSocket();
		electricalSocket.plugIn(ukAdapter);

	}

}
