package com.cegeka.designpatterns.structural.adapter;

public interface GermanPlugConnector {

    public void giveElectricity();
}
