package com.cegeka.designpatterns.structural.adapter;

public interface UKPlugConnector {
	public void provideElectricity();
}
