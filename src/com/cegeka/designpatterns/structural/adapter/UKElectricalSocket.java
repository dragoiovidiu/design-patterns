package com.cegeka.designpatterns.structural.adapter;

public class UKElectricalSocket {
	 public void plugIn(UKPlugConnector plug) {
        plug.provideElectricity();
        System.out.println("Providing UK electricity");
    }
}
