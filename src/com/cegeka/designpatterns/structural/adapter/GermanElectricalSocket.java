package com.cegeka.designpatterns.structural.adapter;

public class GermanElectricalSocket {

    public void plugIn(GermanPlugConnector plug) {
        plug.giveElectricity();
    }
}