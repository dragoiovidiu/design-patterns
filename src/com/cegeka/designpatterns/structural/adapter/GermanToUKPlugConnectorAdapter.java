package com.cegeka.designpatterns.structural.adapter;

public class GermanToUKPlugConnectorAdapter implements UKPlugConnector {

	  private GermanPlugConnector plug;

	    public GermanToUKPlugConnectorAdapter(GermanPlugConnector plug) {
	    	System.out.print("UK adapter = ");
	        this.plug = plug;
	    }

	    @Override
	    public void provideElectricity() {
	        plug.giveElectricity();
	    }


}