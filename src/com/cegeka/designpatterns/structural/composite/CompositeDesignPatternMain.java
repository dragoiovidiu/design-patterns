package com.cegeka.designpatterns.structural.composite;

public class CompositeDesignPatternMain {

	 public static void main(String[] args) {
		 
		 //leafs
		  Employee emp1=new Developer("John", 10000);
		  Employee emp2=new Developer("David", 15000);
		  
		  //composite
		  Employee manager1=new Manager("Daniel",25000);
		  manager1.add(emp1);
		  manager1.add(emp2);
		  
		  //leaf
		  Employee emp3=new Developer("Michael", 20000);
		  
		  //composite
		  Manager generalManager=new Manager("Mark", 50000);
		  generalManager.add(emp3);
		  generalManager.add(manager1);
		  generalManager.print();
	 }
		

}
