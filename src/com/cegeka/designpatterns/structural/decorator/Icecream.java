package com.cegeka.designpatterns.structural.decorator;

public interface Icecream {
	public String makeIcecream();
}
