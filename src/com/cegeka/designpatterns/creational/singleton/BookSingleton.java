package com.cegeka.designpatterns.creational.singleton;

public class BookSingleton {
	private String author = "Gamma, Helm, Johnson, and Vlissides";
	private String title = "Design Patterns";
	private static BookSingleton book = null;
    private static boolean isLoanedOut = false;
	
	private BookSingleton() {
		//do nothing
	}
	
	public static BookSingleton borrowBook() {
		if (isLoanedOut == false) {
			if (book == null) {
				book = new BookSingleton();
			}
			
			isLoanedOut = true;
			return book;
		} else {
			return null;
		}
	}
	
    public void returnBook(BookSingleton $bookReturned) {
        isLoanedOut = false;
    }
    
    public String getAuthor() {
    	return this.author; 
    }
    
    public String getTitle() {
    	return this.title;
    }

    public String getAuthorAndTitle() {
      return String.format("%s by %s",title, author);
    }

}
