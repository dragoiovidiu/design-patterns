package com.cegeka.designpatterns.creational.singleton;

public class BookBorrower {
	private BookSingleton borrowedBook;
	private boolean haveBook = false;
	public BookBorrower() {
		
	}
	
	public String getAuthorAndTitle() {
		if (haveBook == true) {
			return borrowedBook.getAuthorAndTitle();
		} else {
			return "I don't have the book";
		}
	}
	
	public void borrowBook() {
		borrowedBook = BookSingleton.borrowBook();
		
		if (borrowedBook == null ) {
			this.haveBook = false;
		} else {
			this.haveBook = true;
		}
	}
	
	public void returnBook() {
		borrowedBook.returnBook(borrowedBook);
	}	
	
}
