package com.cegeka.designpatterns.creational.builder;

public class Product {
	
	private String color;
	private String price;	
	
	public Product(String color, String price) {
		super();
		this.color = color;
		this.price = price;
	}
	
}
