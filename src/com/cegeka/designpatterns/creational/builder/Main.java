package com.cegeka.designpatterns.creational.builder;

public class Main {
	public static void main(String[] args) {
		Product p = new Builder()
				.withColor("red")
				.withPrice("100")
				.build();

		System.out.println(p);
	}
}
