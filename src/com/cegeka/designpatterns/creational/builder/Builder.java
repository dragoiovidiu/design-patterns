package com.cegeka.designpatterns.creational.builder;


// The builder pattern provides a build object which is used to construct a complex object called the product. It encapsulates the logic of constructing the different pieces of the product.

public class Builder {
	private String color;
	private String price;	
	
	public Builder withPrice(String price) {
		this.price = price;
		return this;
	}
	
	public Builder withColor(String color) {
		this.color = color;
		return this;
	}
	
	public Product build() {
		return new Product(color, price);
	}
}
