package com.cegeka.designpatterns.creational.factorymethod;

public abstract class Smartphone {
	abstract String getPrice();
}
