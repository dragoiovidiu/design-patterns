package com.cegeka.designpatterns.creational.factorymethod;

public class Ios extends Smartphone {
	public String getPrice() {
		return "50";
	}
}
