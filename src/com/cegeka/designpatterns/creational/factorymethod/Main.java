package com.cegeka.designpatterns.creational.factorymethod;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SmartphoneFactory factory = new SmartphoneFactory();
		Smartphone androidPhone = factory.buildSmartphone(SmartphoneType.ANDROID);
		
		System.out.println(androidPhone.getPrice());
	}

}
