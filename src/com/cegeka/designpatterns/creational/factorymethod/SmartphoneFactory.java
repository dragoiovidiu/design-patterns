package com.cegeka.designpatterns.creational.factorymethod;

public class SmartphoneFactory {
	public Smartphone buildSmartphone(SmartphoneType smartphoneType) {
		
		switch(smartphoneType) {
			case ANDROID :
				return new Android();
			case WINDOWS :
				return new Windows();
			case IOS :
				return new Ios();
			
		}
		
		return null;
	}
}
