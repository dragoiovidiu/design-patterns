package com.cegeka.designpatterns.creational.factorymethod;

public enum SmartphoneType {
	ANDROID, IOS, WINDOWS
}
